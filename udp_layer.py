#!/usr/bin/python3
"""
Create an UDP socket ready for broadcast.

The same port can be opened by multiple processes.

The spy class simply displays the broadcasted messages.

copyright Yann GOUY
"""

import socket
import time


_TIMEOUT = 0.5


class Com:

    def __init__(self, port=7777):
        """
        create a communication object
        it has 2 sockets:
          - sock_rx to receive requests, annotations and responses
          - sock_tx to transmit requests, annotations and responses
        """
        self._port = port

        # rx
        self.sock_rx = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.sock_rx.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock_rx.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        self.sock_rx.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_TTL, 1)
        self.sock_rx.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_LOOP, 1)
        self.sock_rx.settimeout(_TIMEOUT)
        self.sock_rx.bind(('', self._port))

        # tx
        self.sock_tx = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock_tx.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    def dup(self):
        """duplicate the socket for UDP connection"""
        return Com(self._port)

    def recv(self):
        """receive messages"""
        try:
            data, addr = self.sock_rx.recvfrom(1024)
            # print('from %s:%s, msg: %s' % (addr[0], addr[1], data.decode()))
        except socket.timeout:
            return None

        return data

    def send(self, data):
        """send messages"""
        try:
            self.sock_tx.sendto(data, ('<broadcast>', self._port))
        except PermissionError:
            # ignore PermissionError since another shall have responded
            # in the broadcast context on the same host
            pass


class Spy:
    """
    simply display every exchanged messages
    """
    def __init__(self, comm):
        self._comm = comm
        self._t_zero = time.time()

    def run(self):
        while True:
            msg = self._comm.recv()
            if msg is None:
                continue

            t_delta = time.time() - self._t_zero

            print(f'@{t_delta:13.6f} s : {msg}')


if __name__ == '__main__':
    print('starting spy...')
    Spy(Com()).run()
