#!/usr/bin/python3
"""
This file shows a simple monitor.

It is instanciated from an example class
that is shared with the client and server.

The framework handles the behaviour difference.

copyright Yann GOUY
"""

import example
import udpbrdcst


# set comm link
comm = udpbrdcst.UdpBrdCst()

# instanciate the example
inst_name = 'example_1'
exmpl = example.Example(comm, inst_name)


# define a simple callback that displays the exchanged messages
# it shall return True as long as the monitoring shall go on
def callback(ctx, exchg):
    print('ctx =', ctx, 'exchg =', exchg)

    ctx[0] += 1
    if ctx[0] > 3:
        return False

    return True


cnt = [0, ]

# wait json rpc calls
print('monitoring', inst_name)
print('-' * 50)
exmpl.monitor(cb=callback, ctx=cnt)
print('done')
