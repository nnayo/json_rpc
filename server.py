#!/usr/bin/python3
"""
This file shows a simple server.

It is instanciated from an example class
that is shared with the client.

The framework handles the behaviour difference.

copyright Yann GOUY
"""

import example
import com_layer


# set comm link
comm = com_layer.Com()

# instanciate the example
exmpl = example.Example(comm, 'example_1')

# wait json rpc calls
exmpl.run()
