"""
basic json rpc implementation

2 decorators are provided to specify request and annotation.

The class JsonRpc shall be inheritated
The subclass shall have its request(s) and/or annotation(s) decorated.

There are 3 usage modes:
    - server:
        - instanciate the object
        - call its run() method
        The RPC calls will be responded.
    - client:
        - instanciate the object
        - call its request(s) and annotation(s) (decorated methods)
        The RPC messages will be exchanged and the request will provide the
        result as the returned value.
    - monitor:
        - instanciate the object
        - call the monitor() method with a context and a callback function
        Each time, an complete exchange is done, the callback will be called
        with content of the echange.

See the provided examples for the 3 modes.

WARNING: by now, multipacket messages are not handled


protocol is based on https://en.wikipedia.org/wiki/JSON-RPC#Implementations

copyright Yann GOUY
"""

import json
import inspect
import mathutils as mu

import unittest
from multiprocessing import Process
import time
import os
import sys


DEBUG = False

if DEBUG:
    time_start = time.time()

    def trace(msg, *args, **kwargs):
        print('@{:13.6f}'.format(time.time() - time_start),
              os.getpid(), msg, *args, **kwargs)
        sys.stdout.flush()

else:
    def trace(msg, *args, **kwargs):
        pass


def request(meth):
    """to be used as a decorator to set a method as a json rpc request"""
    meth.request = True
    return meth


def annotation(meth):
    """to be used as a decorator to set a method as a json rpc annotation"""
    meth.annotation = True
    return meth


def _exception(meth, excpt, comm):
    """
    helper function that throws an exception annotation
    when an exception occurs when handling the given method
    """
    req = json.dumps({
        'jsonrpc': '2.0',
        'method': 'EXCEPTION in ' + meth,
        'params': str(excpt),
        },
        cls=MathutilsEncoder
        )

    # send the request as long as it is not responded
    comm.send(req.encode())


class MathutilsEncoder(json.JSONEncoder):
    """json encoder for Vector and Quaternion mathutils objects"""
    def default(self, obj):
        """"""
        if isinstance(obj, mu.Vector):
            return {
                    '__type__': 'Vector',
                    'x': obj[0],
                    'y': obj[1],
                    'z': obj[2],
                    }

        if isinstance(obj, mu.Quaternion):
            return {
                    '__type__': 'Quaternion',
                    'q0': obj[0],
                    'q1': obj[1],
                    'q2': obj[2],
                    'q3': obj[3],
                    }

        # usual object, so use the default encoder
        super().default(obj)


class MathutilsDecoder(json.JSONDecoder):
    """json decoder for Vector and Quaternion mathutils objects"""
    def __init__(self):
        super().__init__(object_hook=self.mu_decoder)

    def mu_decoder(self, obj):
        if '__type__' not in obj:
            return obj

        if obj['__type__'] == 'Vector':
            return mu.Vector((obj['x'], obj['y'], obj['z']))

        if obj['__type__'] == 'Quaternion':
            return mu.Quaternion((obj['q0'], obj['q1'], obj['q2'], obj['q3']))

        return obj


class JsonRpc:
    def __init__(self, comm, instance_name):
        """
        The comm interface shall provide:
         - recv and send methods for message exchanges,
         - copy method to duplicate the link
        The instance name is required to correctly filter
        the incoming messages,
        in the case where multiple instances of the same class are created.
        """
        self._request_id = id(self) << 32  # initial request id
        self._comm = comm
        self._with_json_rpc = True
        self._inst_name = instance_name + '.'
        self._is_running = True

        trace('calling', self._inst_name, '__init__')

    def run_as_spawn(self):
        """
        serving loop for a server
        if the run method is to be called via a spawn process,
        the comm link shall be duplicated to prevent socket lock-down
        """
        self._comm = self._comm.dup()

        self.run()

    def run(self):
        """
        serving loop for a server
        """
        # send annotation to signal start
        # create the json rpc message
        req = json.dumps({
            'jsonrpc': '2.0',
            'method': self._inst_name + 'server start-up indication',
            'params': None
            },
            cls=MathutilsEncoder
            )

        trace(self._inst_name, 'sending start annotation')

        # send the message
        self._comm.send(req.encode())

        # some "optimization" due to __getattribute__ usage
        comm = self._comm
        filtr = self._filter_instance
        handle = self._handle

        import sys
        while self._is_running:
            # wait an incoming message
            msg = comm.recv()
            if msg is None:
                continue

            trace('run() receiving:', msg)

            # convert message to dict
            json_msg = json.loads(msg, cls=MathutilsDecoder)

            # filter out response
            if 'result' in json_msg.keys():
                # there is apparently nothing useful to do
                trace('ignoring result', msg)
                continue

            # filter out instance mane
            json_msg = filtr(json_msg)
            if json_msg is None:
                trace('filtering on bad instance name', msg)
                continue

            # filter out state annotation
            # (the one that is automatically sent at server start-up)
            if 'start-up indication' in json_msg['method']:
                continue

            # handle the received message
            handle(json_msg)

    def _filter_instance(self, json_msg):
        """
        filter the message on instance name
        and return it if it is correct
        """
        # filter the message
        if not json_msg['method'].startswith(self._inst_name):
            return

        # strip the method name from the instance name
        # since only the method name is known for getattr()
        json_msg['method'] = json_msg['method'].replace(self._inst_name, '')

        # check if method name is valid and always ignore state annotation
        if '.' in json_msg['method']:
            return

        return json_msg

    def _handle(self, json_req):
        """handle the message and send the response if needed"""
        trace('_handle():', json_req['method'])

        # check if request or annotation
        if 'id' in json_req.keys():
            trace('--> request: ', json_req)

            # call request without json rpc encapsulation
            self._with_json_rpc = False

            request = getattr(self, json_req['method'])
            result = request(*json_req['params'].values())

            self._with_json_rpc = True

            # build the response
            json_rsp = {}
            json_rsp['jsonrpc'] = json_req['jsonrpc']
            json_rsp['id'] = json_req['id']
            json_rsp['result'] = result

            rsp = json.dumps(json_rsp, cls=MathutilsEncoder)

            # send the response
            self._comm.send(rsp.encode())

        else:
            trace('--> annotation: ', json_req)
            # call annotation without json rpc encapsulation
            self._with_json_rpc = False

            annotation = getattr(self, json_req['method'])
            annotation(*json_req['params'].values())

            self._with_json_rpc = True

    def _filter_id(self):
        """
        filter the received messages
        since it's running in a boardcast context
        """
        # some "optimization" due to __getattribute__ usage
        comm = self._comm
        req_id = self._request_id

        while True:
            msg = comm.recv()
            if msg is None:
                trace('recv timeout')
                return None

            json_msg = json.loads(msg, cls=MathutilsDecoder)

            if 'result' in json_msg and json_msg['id'] == req_id:
                return json_msg

            trace('filtered msg = ', json_msg)

    def _flush(self):
        """
        since the messages are broadcasted,
        they are auto-echoed
        in the annotation case, it is worth flushing the link
        to prevent huge memory usage if only annotation is used
        """
        self._comm.recv()

    def __getattribute__(self, attr):
        """
        override usual method access to change the behaviour
        for decorated request and annotation
        """
        try:
            meth = super().__getattribute__(attr)
        except AttributeError as excpt:
            _exception(self._inst_name + attr,
                       excpt, self._comm)
            raise

        # if the object has no __dict__ field, return it immediatly
        d = None
        try:
            d = meth.__dict__
        except AttributeError:
            return meth

        if 'request' in d:
            trace('call request:', self._inst_name + meth.__name__)

            # add json sugar around the method
            def json_request(*params):
                """
                it handles all the json rpc formatting:
                 - increment the request id,
                 - send the message,
                 - wait the response,
                 - return the value
                """
                if not self._with_json_rpc:
                    try:
                        return meth(*params)
                    except TypeError as excpt:
                        _exception(self._inst_name + meth.__name__,
                                   excpt, self._comm)
                        return None

                # format method parameters
                args = {}
                sig = inspect.signature(meth)
                for z in zip(sig.parameters.keys(), params):
                    args[z[0]] = z[1]

                # create the json rpc message
                req = json.dumps({
                    'jsonrpc': '2.0',
                    'method': self._inst_name + meth.__name__,
                    'params': args,
                    'id': self._request_id,
                    },
                    cls=MathutilsEncoder
                    )

                # send the request as long as it is not responded
                json_rsp = None
                while json_rsp is None:
                    # send the message
                    self._comm.send(req.encode())

                    # wait the response
                    json_rsp = self._filter_id()

                    if json_rsp is None:
                        trace('retrying req = ', req)

                trace('json_rsp = ', json_rsp)

                # update request id since it has been received
                self._request_id += 1

                # return only the result
                return json_rsp['result']

            return json_request

        if 'annotation' in d:
            trace('call annotation:', self._inst_name + meth.__name__)

            # add json sugar around the method
            def json_annotation(*params):
                """
                it handles all the json rpc formatting
                it simply send the message
                """
                if not self._with_json_rpc:
                    try:
                        meth(*params)
                    except TypeError as excpt:
                        _exception(self._inst_name + meth.__name__,
                                   excpt, self._comm)
                    return

                args = {}
                sig = inspect.signature(meth)
                for z in zip(sig.parameters.keys(), params):
                    args[z[0]] = z[1]

                # create the json rpc message
                req = json.dumps({
                    'jsonrpc': '2.0',
                    'method': self._inst_name + meth.__name__,
                    'params': args
                    },
                    cls=MathutilsEncoder
                    )

                # send the message
                self._comm.send(req.encode())

            return json_annotation

        return meth

    def monitor(self, cb, ctx):
        """
        monitor the exchanges
        on reception of a complete request exchange or on an annotation,
        the callback is call with the given context and the complete exchange
        """
        # some "optimization" due to __getattribute__ usage
        comm = self._comm
        filtr = self._filter_instance

        # for matching requests and responses
        ids = {}

        while True:
            # wait an incoming message
            msg = comm.recv()
            if msg is None:
                continue

            # monitor the received message
            trace('monitor():', msg)

            # convert message to dict
            json_msg = json.loads(msg, cls=MathutilsDecoder)

            # try to match response with a request
            if 'result' in json_msg.keys():
                res_id = json_msg['id']
                if res_id not in ids:
                    continue

                # a match is found, prepare the callback exchange
                exchng = ids[res_id]
                exchng['result'] = json_msg['result']

                # if callback returns False, stop monitoring
                if cb(ctx, exchng) is False:
                    return

                # delete response id from known ids
                del ids[res_id]

                continue

            # so it's a request an an annotation
            # filter out instance mane
            json_msg = filtr(json_msg)
            if json_msg is None:
                continue

            # if it's an annotation
            if 'id' not in json_msg.keys():
                exchng = {
                    'method': json_msg['method'],
                    'params': json_msg['params']
                }

                # if callback returns False, stop monitoring
                if cb(ctx, exchng) is False:
                    return

                continue

            # finally, it's a request
            # add its id to the known ids
            ids[json_msg['id']] = {
                'method': json_msg['method'],
                'params': json_msg['params'],
            }

    @request
    def ping(self):
        """send a ping, if the server is running, it replies with a pong"""
        return 'pong'

    @request
    def kill(self):
        """remote shutdown of the server"""
        self._is_running = False

        return 'shuting down'
