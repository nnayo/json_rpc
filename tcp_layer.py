#!/usr/bin/python3
"""
Create an TCP server
that will broadcadt a message from one end to the others.

The Comm class permits the dialog with the server.

The server always displays the broadcasted messages.

copyright Yann GOUY
"""

import socket
import time
import select
import json
import re


_DEFAULT_PORT = 7777
_TIMEOUT_S = 0.5
_TIMEOUT_MS = 1000 * _TIMEOUT_S
_MAX_BUF_SIZE = 2048
_HOST = 'localhost'


class Com:
    def __init__(self, port=_DEFAULT_PORT, host=_HOST):
        """
        create a communication object
        it has only a TCP socket
        """
        self._port = port

        # reception fifo
        self._fifo = []

        # socket creation
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self._sock.connect((host, self._port))
        except ConnectionRefusedError:
            print('please start the TCP server')
            raise
        self._sock.setblocking(True)

        # poll creation
        self._poll = select.poll()
        self._poll.register(self._sock,
                            select.POLLIN | select.POLLERR | select.POLLHUP)

    def dup(self):
        """duplicate the socket for TCP connection"""
        return Com(self._port)

    def recv(self):
        """receive messages"""
        # if there is at least 1 message in fifo
        if len(self._fifo):
            return self._fifo.pop(0)

        # there only 1 registered socket
        evts = self._poll.poll(_TIMEOUT_MS)

        if evts == []:
            return

        sock, evt = evts[0]
        if sock != self._sock.fileno():
            return

        # data to read
        if evt & select.POLLIN:
            data = self._sock.recv(_MAX_BUF_SIZE)

            # if multi messages are packed in the same frame
            # it is needed to split them
            # and to enqueue them in the fifo
            while len(data):
                try:
                    json.loads(data)
                    self._fifo.append(data)
                    data = b''

                except json.JSONDecodeError as ex:
                    # retrieve the json message end
                    mtch = re.match('.*char (?P<end>\d+)', str(ex))
                    end = int(mtch.group('end'))

                    self._fifo.append(data[0:end])
                    data = data[end:]

            return self._fifo.pop(0)

        if evt & select.POLLHUP:
            print('recv HUP')

        if evt & select.POLLERR:
            print('recv ERR')

    def send(self, data):
        """send messages"""
        self._sock.send(data)


class Server:
    """
    wait connections from client
    broadcast messages to all client
    display every exchanged messages
    """
    def __init__(self, port=_DEFAULT_PORT):
        self._port = port
        self._t_zero = time.time()

        # init connection socket
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.setblocking(True)
        self._sock.bind(('', port))
        self._sock.listen(10)

        # init connected socket dict
        self._socks = {}

        # prepare the polling
        self._poll = select.poll()
        self._register(self._sock),

    def _register(self, sock):
        """register the given socket to the poll"""
        self._poll.register(sock,
                            select.POLLIN | select.POLLERR | select.POLLHUP)

    def _connection(self, evt):
        """new connection"""
        print('connection socket event')
        # new connection
        if evt & select.POLLIN:
            conn, addr = self._sock.accept()
            ip, port = str(addr[0]), str(addr[1])
            print(f'new connection from {ip}:{port} : {conn}')
            self._socks[conn.fileno()] = conn
            self._register(conn)

        # event HUP
        if evt & select.POLLHUP:
            print('connection HUP')

        # event ERR
        if evt & select.POLLERR:
            print('connection ERR')

    def _brdcst(self, sock_fd, msg):
        """broadcast msg to all connected sockets"""
        for s in self._socks.values():
            s.send(msg)

    def _connected(self, sock_fd, evt):
        """handling a connected socket"""
        sock = self._socks[sock_fd]

        # if data to read
        if evt & select.POLLIN:
            # get the data
            try:
                msg = sock.recv(_MAX_BUF_SIZE)
            except ConnectionResetError:
                # force empty msg and simulate error
                msg = b''
                evt |= select.POLLERR

            # if data
            if len(msg):
                # display it with the time
                t_delta = time.time() - self._t_zero
                print(f'@{t_delta:13.6f} s : {sock_fd} --> {msg}')

                # broadcast to all connected sockets
                self._brdcst(sock, msg)

            else:
                # no data, the socket is disconnecting
                evt |= select.POLLHUP

        if evt & select.POLLERR:
            print('error from', sock)
            sock.close()
            self._poll.unregister(sock_fd)
            del self._socks[sock_fd]

            return

        # if disconnection
        if evt & select.POLLHUP:
            print('disconnection from', sock)
            try:
                sock.shutdown(socket.SHUT_RDWR)
            except OSError:  # [Errno 107] Transport endpoint is not connected
                pass
            sock.close()
            self._poll.unregister(sock_fd)
            del self._socks[sock_fd]

    def run(self):
        while True:
            # wait for any event on the registered sockets
            evts = self._poll.poll()

            for sock_fd, evt in evts:
                # event on the connection socket
                if sock_fd == self._sock.fileno():
                    self._connection(evt)

                    continue

                # event on one of the connected socket
                self._connected(sock_fd, evt)


if __name__ == '__main__':
    print('starting server...')
    srv = Server()
    srv.run()
