#!/usr/bin/python3
"""
This file mixes the server and client examples
spawing the server as a sub-process.

It is instanciated from an example class.

copyright Yann GOUY
"""

import example
import com_layer

from multiprocessing import Process
import time
import os
import sys


time_start = time.time()


def trace(msg, *args, **kwargs):
    print('@{:13.6f}'.format(time.time() - time_start),
          os.getpid(), msg, *args, **kwargs)
    sys.stdout.flush()


# instanciate the example server
exmpl = example.Example(com_layer.Com(), 'example_1')

# start server as the sub-process
# if the run method is called without specifying it's a sub-process
# the same socket will be used for communication.
# in the TCP case, it's a dead-lock:
# the run method will block forever
p = Process(target=exmpl.run_as_spawn, name='exmpl')
p.start()

# wait for the server to start
while True:
    trace('ping -->')
    if exmpl.ping() == 'pong':
        break

# send an annotation
trace('annotation -->')
exmpl.inc(7)

# send a request and get the response
trace('request -->')
res = exmpl.cnt(1, 2)
trace('cnt = ', res)
assert(res == [7, 2])

# send a request and get the response
trace('request -->')
res = exmpl.cnt(3, 5)
trace('cnt = ', res)
assert(res == [7, 15])

time.sleep(1)
p.terminate()
