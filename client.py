#!/usr/bin/python3
"""
This file shows a simple client.

It is instanciated from an example class
that is shared with the server.

The framework handles the behaviour difference.

copyright Yann GOUY
"""

import example
import com_layer


# set comm link
comm = com_layer.Com()

# instanciate the exmaples
exmpl_1 = example.Example(comm, 'example_1')
exmpl_2 = example.Example(comm, 'example_2')

# wait for the server to start
while True:
    print('ping -->')
    if exmpl_1.ping() == 'pong':
        break

# send an annotation
print('annotation -->')
exmpl_1.inc(7)

# send a request and get the response
print('request -->')
res = exmpl_1.cnt(1, 2)
print('cnt = ', res)

# send a request and get the response
print('request -->')
res = exmpl_1.cnt(3, 5)
print('cnt = ', res)

# send an annotation
print('annotation -->')
exmpl_2.inc(13)

# send a request and get the response
print('request -->')
res = exmpl_1.cnt(11, 17)
print('cnt = ', res)
