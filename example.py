"""
This example class shows how to implement an json RPC request and annotation.

Simply a subclass from the framework class.

The same class can be used for the client and the server.
See the server.py and client.py files.

copyright Yann GOUY
"""

import jsonrpc


class Example(jsonrpc.JsonRpc):
    """example with json rpc callable methods"""
    def __init__(self, comm, instance_name):
        """
        the comm parameter is the communication link
        the instance name is used for message filtering
        don't forget to call the parent constructor
        """
        super().__init__(comm, instance_name)
        self._annotation_cnt = 0

    @jsonrpc.request
    def cnt(self, arg1, arg2):
        """
        the method is declared a request via the decorator
        it shall return a value
        """
        print('cnt = ', self._annotation_cnt, arg1, arg2)
        return (self._annotation_cnt, arg1 * arg2)

    @jsonrpc.annotation
    def inc(self, arg1):
        """
        the method is declared an annotation via the decorator
        any returned value is useless and ignored
        """
        self._annotation_cnt += arg1
        print('inc = ', arg1)
